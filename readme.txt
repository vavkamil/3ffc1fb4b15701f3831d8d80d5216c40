=== covid19-plugin-wp ===
Contributors: vavkamil
Tags: covid19, covid, coronavirus
Requires at least: 3.9
Requires PHP: 5.3
Tested up to: 5.8
Stable tag: 1.0.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin adds a custom widget to display Covid19 statistics for a selected country.

== Description ==

### covid19-plugin-wp

Display Covid19 statistics for any country:

#### Free to use, no registration needed

All the data are provided for free via https://apify.com/covid-19. No registration or API key is needed. This plugin works out of the box.

#### Latest data

Some of these coronavirus APIs are no longer actively maintained, as infections have dropped in those countries thanks to vaccination. Right now, there are the latest data for more than 45+ countries.

== Installation ==

1. Install covid19-plugin-wp automatically or by uploading the ZIP file. 
2. Activate the covid19-plugin-wp through the 'Plugins' menu in WordPress. covid19-plugin-wp is now activated.
3. Go to the Appearance > Widgets and configure the plugin based on your needs.

== Frequently Asked Questions ==

= APIs for COVID-19 statistics =

All our COVID-19 APIs are open-source and free to use without any limitations. But if you find them useful, we would appreciate it if you link to https://apify.com/covid-19

== Changelog ==

= 1.0.1 - November 16 =
- Replaced `file_get_contents()` with `wp_remote_get()`

= 1.0 - November 10, 2021 =
- An initial release
