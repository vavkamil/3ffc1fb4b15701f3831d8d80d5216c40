<?php

/**
 * Plugin Name:       covid19-plugin-wp
 * Plugin URI:        https://wordpress.org/plugins/covid19-plugin-wp
 * Description:       This plugin adds a custom widget to display Covid19 statistics for a selected country.
 * Version:           1.0.1
 * Author:            @vavkamil
 * Author URI:        https://vavkamil.cz
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       covid19-plugin-wp
 */

// ^ Header Requirements
// https://developer.wordpress.org/plugins/plugin-basics/header-requirements/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

// Adds Covid19_Plugin_WP widget.
class Covid19_Plugin_WP extends WP_Widget {

    // Register widget with WordPress.
    function __construct() {
        parent::__construct(
            'covid19-plugin-wp', // Base ID
            esc_html__( 'Covid 19 statistics', 'covid19-plugin-wp' ), // Name
            array( 'description' => esc_html__( 'Displays Covid 19 statistics', 'covid19-plugin-wp' ), ) // Args
        );
    }

    // The widget form (for the backend )
    public function form( $instance ) {

        // Set widget defaults
        $defaults = array(
            'title'    => '',
            'select'   => '',
        );
        
        // Parse current settings with defaults
        extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

        <?php // Widget Title ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <?php // Dropdown for selecting country ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'select' ); ?>"><?php _e( 'Please select a country to display the Covid 19 data for', 'text_domain' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'select' ); ?>" id="<?php echo $this->get_field_id( 'select' ); ?>" class="widefat">
            <?php
                $content = wp_remote_retrieve_body( wp_remote_get( "https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true" ));
                $arr = json_decode($content);
                foreach($arr as $item) {
                    $name = $item->country;
                    echo '<option value="' . esc_attr( $name ) . '" id="' . esc_attr( $name ) . '" '. selected( $select, $name, false ) . '>'. esc_attr( $name ) . '</option>';
                }
            ?>
            </select>
        </p>

    <?php }

    // Update widget settings
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
        $instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
        return $instance;
    }

    // Display the widget
    public function widget( $args, $instance ) {

        extract( $args );

        // Check the widget options
        $title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
        $select   = isset( $instance['select'] ) ? $instance['select'] : '';

        // WordPress core before_widget hook (always include )
        echo $before_widget;

        // Display the widget
        echo '<div class="widget-text wp_widget_plugin_box">';

            // Display widget title if defined
            if ( $title ) {
                echo $before_title . $title . $after_title;
            }

            // Display Covid 19 data for selected country
            if ( $select ) {
                $content = wp_remote_retrieve_body( wp_remote_get( "https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true" ));
                $data = json_decode($content);
                foreach($data as $item) {
                    if($item->country == $select) {
                        echo "<p><strong>Infected</strong>: " . number_format(intval($item->infected), 0, '', ' ') . "</p>";
                        echo "<p><strong>Tested</strong>: " . number_format(intval($item->tested), 0, '', ' ') . "</p>";
                        echo "<p><strong>Recovered</strong>:"  . number_format(intval($item->recovered), 0, '', ' ') . "</p>";
                        echo "<p><strong>Deceased</strong>: " . number_format(intval($item->deceased), 0, '', ' ') . "</p>";
                    }
                }
            }

        echo '</div>';

        // WordPress core after_widget hook (always include )
        echo $after_widget;

    }

}

// Register the widget
function my_register_covid19_plugin_wp() {
    register_widget( 'Covid19_Plugin_WP' );
}

add_action( 'widgets_init', 'my_register_covid19_plugin_wp' );